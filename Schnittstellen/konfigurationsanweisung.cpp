#include "Schnittstellen/konfigurationsanweisung.h"

Konfigurationsanweisung::Konfigurationsanweisung(double backzeit, double ofenTemperatur):
    m_backzeit(backzeit),
    m_ofenTemperatur(ofenTemperatur)
{

}

double Konfigurationsanweisung::backzeit() const
{
    return m_backzeit;
}

double Konfigurationsanweisung::ofenTemperatur() const
{
    return m_ofenTemperatur;
}
