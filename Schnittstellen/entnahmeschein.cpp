#include "Schnittstellen/entnahmeschein.h"

Entnahmeschein::Entnahmeschein(::std::map<int, double> initializer):
    m_backing(initializer)
{

}

::std::map<int, double> &Entnahmeschein::backing()
{
    return m_backing;
}

const ::std::map<int, double> &Entnahmeschein::backing() const
{
    return m_backing;
}
