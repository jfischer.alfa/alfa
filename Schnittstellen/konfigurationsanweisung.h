#ifndef KONFIGURATIONSANWEISUNG_H
#define KONFIGURATIONSANWEISUNG_H


/**
 * @brief Konfigurationsanweisung fuer Backstrasse
 */
class Konfigurationsanweisung
{
public:
    /**
     * @brief Konstruktor
     * @param backzeit einzustallende Backzeit
     * @param ofenTemperatur einzustallende Ofentemperatur
     */
    Konfigurationsanweisung(double backzeit, double ofenTemperatur);

    double backzeit() const;
    double ofenTemperatur() const;

private:
    double m_backzeit;
    double m_ofenTemperatur;
};

#endif // KONFIGURATIONSANWEISUNG_H
