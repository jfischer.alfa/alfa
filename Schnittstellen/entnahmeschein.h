#ifndef ENTNAHMESCHEIN_H
#define ENTNAHMESCHEIN_H

#include <map>

/**
 * @brief Entnahmeschein
 * @descipription dieses Dokument stellt einen Auftrag fuer das Lager dar, Zutaten in entsprechender Menge herauszugeben
 */
class Entnahmeschein
{
public:
    /**
     * @brief Konstruktor
     * @param initializer Zuordnung Kennungen fuer Zutaten mit der benoetigten aus dem Lager zu holenden Menge
     */
    explicit Entnahmeschein(::std::map<int, double> initializer);

    /**
      * @brief getter fuer die Zuordnung der Kennungen der Zutaten zu den benoetigten Mengen
      * @return Zuordnung Kennungen der Zutaten zu benoetigten aus dem Lager hervorzuholenden Mengen
      * */
    ::std::map<int, double> &backing();

    /**
     * @brief derselbe getter fuer const context
     * @return Zuordnung Kennungen der Zutaten zu benoetigten aus dem Lager hervorzuholenden Mengen
     */
    const ::std::map<int, double> &backing() const;

private:
    ::std::map<int, double> m_backing;
};

#endif // ENTNAHMESCHEIN_H
