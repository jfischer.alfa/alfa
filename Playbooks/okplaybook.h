#ifndef OKPLAYBOOK_H
#define OKPLAYBOOK_H

#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"


class OKPlaybook : public IPlaybook
{
public:

    /**
     * @brief Implementation der Oberklasse
     * @description Nicht direkt aufrufen!
     * @param pPhysik
     */
    void play(Physik *pPhysik);
};

#endif // OKPLAYBOOK_H
