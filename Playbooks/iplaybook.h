#ifndef IPLAYBOOK_H
#define IPLAYBOOK_H

class Physik;

class IPlaybook
{
public:
    virtual ~IPlaybook();

    virtual void play(Physik *) =  0;

};

#endif // IPLAYBOOK_H
