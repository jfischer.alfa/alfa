#ifndef SPEEDPLAYBOOK_H
#define SPEEDPLAYBOOK_H

#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"


class SpeedPlaybook : public IPlaybook
{
public:

    const double speed =  768.0;

    SpeedPlaybook();

    /**
     * @brief Implementation der Oberklasse
     * @description Nicht direkt aufrufen!
     * @param pPhysik
     */
    void play(Physik *pPhysik);

};

#endif // SPEEDPLAYBOOK_H
