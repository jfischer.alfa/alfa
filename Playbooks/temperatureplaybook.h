#ifndef TEMPERATUREPLAYBOOK_H
#define TEMPERATUREPLAYBOOK_H

#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"


class TemperaturePlaybook : public IPlaybook
{
public:

    const double temperature =  750.0;


    TemperaturePlaybook();

    /**
     * @brief Implementation der Oberklasse
     * @description Nicht direkt aufrufen!
     * @param pPhysik
     */
    void play(Physik *pPhysik);

};

#endif // TEMPERATUREPLAYBOOK_H
