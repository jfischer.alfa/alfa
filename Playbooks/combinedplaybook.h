#ifndef COMBINEDPLAYBOOK_H
#define COMBINEDPLAYBOOK_H

#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"


class CombinedPlaybook : public IPlaybook
{
public:

    const double temperature =  384.0;


    CombinedPlaybook();

    /**
     * @brief Implementation der Oberklasse
     * @description Nicht direkt aufrufen!
     * @param pPhysik
     */
    void play(Physik *pPhysik);

};

#endif // COMBINEDPLAYBOOK_H
