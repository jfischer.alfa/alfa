#ifndef STOPPLAYBOOK_H
#define STOPPLAYBOOK_H

#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"



class StopPlaybook : public IPlaybook
{
public:
    StopPlaybook();

    /**
     * @brief Implementation der Oberklasse
     * @description Nicht direkt aufrufen!
     * @param pPhysik
     */
    void play(Physik *pPhysik);

};

#endif // STOPPLAYBOOK_H
