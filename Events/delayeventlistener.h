#ifndef DELAYEVENTLISTENER_H
#define DELAYEVENTLISTENER_H

#include "Events/events.t"

/**
 * @brief versendet event, sobald Interessen einverstanden sind
 * @description Interessenten, die erreichen moechten, dass Objekte
 *    dieser Klasse den Event senden, rufen die Methode createEventListener
 *    auf und speichern dessen Ergebnis.  Die Methode trigger des Ergebnisses
 *    wird aufgerufen, sobald betreffender Interessant einverstanden ist, dass
 *    der Event gesendet wird.  Danach muss derselbe Interessent noch die
 *    reset()-Methode aufrufen.  Sobald alle Interessenten auf diese Art
 *    sich einverstanden erklaert haben, wird der Event gesendet.
 */
class DelayEventListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer DelayEventListener
     */
    DelayEventListener();

    /** @brief Implementation der entsprechenden Methode der Oberklasse.
     *  @description Nicht direkt aufrufen!
     */
    void trigger(int);

    /** @brief Interessenten rufen diese Methode auf und speichern dessen Ergebnis
     *  @description Durch Aufruf von dessen Methode trigger erklaert Interessent sich
     *     einverstanden, dass Objekt ein Event erzeugt. */
    IEventListener<int> *createEventListener();

    /** @brief getter fuer das zum Versenden des Events benoetigten EventSenders
     */
    EventSender<int> &eventSender();

private:
    EventSender<int> m_eventSender;
    unsigned m_nr_listeners;
};

#endif // DELAYEVENTLISTENER_H
