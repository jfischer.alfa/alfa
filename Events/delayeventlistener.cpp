#include "delayeventlistener.h"

DelayEventListener::DelayEventListener():
    m_eventSender(),
    m_nr_listeners(0)
{

}

void DelayEventListener::trigger(int payload)
{
    m_nr_listeners--;
    if (!m_nr_listeners) {
        m_eventSender.transmit(payload);
    }
}

IEventListener<int> *DelayEventListener::createEventListener()
{
    m_nr_listeners++;
    return this;
}

EventSender<int> &DelayEventListener::eventSender()
{
    return m_eventSender;
}
