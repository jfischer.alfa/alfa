#ifndef EVENTS_T
#define EVENTS_T

#include <vector>

/**
 * @brief generisches Interface IEventListener
 * @description EventSender werden die Methode trigger aufrufen.  Implementiere diese Methode so,
 *   dass EventListener damit auf das Eintreffen eines Events reagiert.
 */
template<class T>
class IEventListener {

public:
  virtual ~IEventListener() {}

  /**
   * @brief abstrakte Methode trigger.  Implementation legt fest, wie EventListener auf Einteffen eines Events reagieren soll
   * @param payload mit eintreffendem Event uebermittelte Daten
   */
  virtual void trigger(T payload) =  0;

};

/**
 * @brief EventListener, der eine Variable beim EventSender mit einer beim EventListener synchronisiert.
 */
template<class T>
class PropertyListener:
   public IEventListener<T> {

public:
   explicit PropertyListener(const T &initializer):
      IEventListener<T>(),
      m_property(initializer),
      m_is_ever_updated(false) {
   }

   /**
    * @brief Implementation der entsprechenden Methode der Oberklasse.
    * @description Nicht direkt aufrufen!
    * @param payload
    */
   void trigger(T payload) {
      if (!is_ever_updated() || payload != m_property) {
         m_property =  payload;
         m_is_ever_updated =  true;
         onPropertyChanged(m_property);
      }
   }

   /**
    * @brief abstrakte Methode onPropertyChanged, die festlegt, wie sich EventListener bei Aenderung der Variable verhalten soll.
    * @param data mit Event uebermittelte Daten
    */
   virtual void onPropertyChanged(T &data) {
   }

   /**
    * @brief getter fuer die vom EventListener verwaltete Variable
    * @return in Variable gespeicherter Wert
    */
   const T get() const {
      return m_property;
   }

   /**
    * @brief ist je ein Event eingetroffen?
    * @description Wenn nicht, dann sind die verwalteten Variablen bei EventSender und EventListener noch nicht synchron
    * @return ist je ein Event eingetroffen?
    */
   bool is_ever_updated() const {
      return m_is_ever_updated;
   }

   /**
     * @brief PropertyListener zuruecksetzen
     */
   void reset() {
       m_is_ever_updated =  false;
   }

private:
   T m_property;
   bool m_is_ever_updated;

};

/**
 * @brief Absenden eines Events.
 * @description Er tut es, in er fuer alle registrierten EventListern die Methode trigger aufruft.
 */
template<class T>
class EventSender {

public:
   EventSender():
      m_eventListeners() {
   }

   /**
    * @brief registriert EventListener
    * @description beim Ausloesen eines Events, wird die Methode trigger aufgerufen
    * @param pListener zu registrierender EventListener
    */
   void addListener(IEventListener<T> *pListener) {
      m_eventListeners.push_back(pListener);
   }

   /**
    * @brief Event versenden
    * @description an alle registrierten EventListener
    * @param payload zum EventListener uebermittelter Wert
    */
   void transmit(T payload) {
      for (
            typename ::std::vector<IEventListener<T> *>::iterator it =  m_eventListeners.begin()
         ;  it != m_eventListeners.end()
         ;  ++it ) {
         (*it) -> trigger(payload);
      }
   }

    /**
     * @brief Registrierungen aller EventListener rueckgaengig machen
     */
    void reset() {
        m_eventListeners.clear();
    }

private:
   ::std::vector<IEventListener<T> *> m_eventListeners;

};

/**
 * @brief zum Synchronisieren von Variablen beim EventListener und EventSender verwendeter EventSender
 */
template<class T>
class PropertySender:
   public EventSender<T> {

public:
    /**
    * @brief Konstruktor fuer PropertySender
    * @param initializer Vorbelegung fuer verwaltete Variable
    */
   explicit PropertySender(const T &initializer):
      EventSender<T>(),
      m_property(initializer),
      m_is_ever_updated(false) {
   }

   /**
    * @brief getter fuer verwaltete Variable
    * @return in Variable enthaltener Wert
    */
   const T get() const {
      return m_property;
   }

   /**
    * @brief setter fuer verwaltete Variable
    * @param payload in Variable zu speichernder Wert
    */
   void set(const T &payload) {
      if (!is_ever_updated() || payload != m_property) {
         m_property =  payload;
         m_is_ever_updated =  true;
         transmit(payload);
      }
   }

   /**
    * @brief hat je ein Event stattgefunden?
    * @description Wenn nicht, dann sind Variablen bei EventSender und EventListener nicht synchron
    * @return hat je ein Event stattgefunden?
    */
   bool is_ever_updated() const {
      return m_is_ever_updated;
   }

private:
   T m_property;
   bool m_is_ever_updated;

};

#endif
