#ifndef BACKKONFIGURATION_H
#define BACKKONFIGURATION_H
#include <map>

/**
 * @brief Ein Rezept enthält Zutaten, Backzeit und -temperatur.
 * @description Wird vom Baeckermeister auf Basis des Auftrags erstellt.
 */
class Rezept
{
public:
    /**
     * @brief Konstruktor
     * @param Backtemperatur
     * @param Backzeit
     * @param Liste der gesamten Zutaten
     */
    Rezept(double temperatur, double backzeit, std::map<int,double> zutatenliste);
    ~Rezept();

    double getBackZeit();
    double getBackTemperatur();
    std::map<int,double> getZutatenliste();
private:
    double backzeit;
    double backtemperatur;
    std::map<int,double> zutatenliste;
};

#endif // BACKKONFIGURATION_H
