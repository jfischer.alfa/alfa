#include "rezept.h"

Rezept::Rezept(double temperatur, double backzeit, std::map<int,double> zutatenlist):
        backzeit{backzeit},
        backtemperatur{temperatur},
    zutatenliste{zutatenlist}

{

}

Rezept::~Rezept()
{

}

double Rezept::getBackZeit()
{
    return this->backzeit;
}
double Rezept::getBackTemperatur()
{
    return this->backtemperatur;
}

std::map<int, double> Rezept::getZutatenliste()
{
    return this->zutatenliste;
}
