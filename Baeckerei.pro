#-------------------------------------------------
#
# Project created by QtCreator 2020-04-23T14:16:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Baeckerei
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    Auftrag/auftrag.cpp \
    Auftrag/logger.cpp \
    Controller/automatstoplistener.cpp \
    Controller/backzeitzuhochlistener.cpp \
    Controller/backzeitzuniedriglistener.cpp \
    Controller/baeckereicontroller.cpp \
    Controller/ofenzuheisslistener.cpp \
    Controller/ofenzukaltlistener.cpp \
    Events/delayeventlistener.cpp \
    Lebensmittel/Baeckermeister.cpp \
    Lebensmittel/Belag.cpp \
    Lebensmittel/form.cpp \
    Lebensmittel/groesse.cpp \
    Lebensmittel/Teig.cpp \
    Lebensmittel/Zutat.cpp \
    PhysicalObjects/Automat/automat.cpp \
    PhysicalObjects/Betrieb/betrieb.cpp \
    PhysicalObjects/Blech/blech.cpp \
    PhysicalObjects/Lager/einlagernlistener.cpp \
    PhysicalObjects/Lager/lager.cpp \
    PhysicalObjects/Laufband/laufband.cpp \
    PhysicalObjects/Laufband/speedlistener.cpp \
    PhysicalObjects/Laufband/stoplistener.cpp \
    PhysicalObjects/Ofen/ofen.cpp \
    PhysicalObjects/Ofen/temperaturelistener.cpp \
    PhysicalObjects/Physik/physik.cpp \
    PhysicalObjects/System/system.cpp \
    Playbooks/combinedplaybook.cpp \
    Playbooks/iplaybook.cpp \
    Playbooks/okplaybook.cpp \
    Playbooks/speedplaybook.cpp \
    Playbooks/stopplaybook.cpp \
    Playbooks/temperatureplaybook.cpp \
    Rezept/rezept.cpp \
    Schnittstellen/entnahmeschein.cpp \
    Schnittstellen/konfigurationsanweisung.cpp

HEADERS += \
        mainwindow.h \
    Auftrag/auftrag.h \
    Auftrag/logger.h \
    Controller/automatstoplistener.h \
    Controller/backzeitzuhochlistener.h \
    Controller/backzeitzuniedriglistener.h \
    Controller/baeckereicontroller.h \
    Controller/ofenzuheisslistener.h \
    Controller/ofenzukaltlistener.h \
    Events/delayeventlistener.h \
    Events/events.t \
    Lebensmittel/Baeckermeister.h \
    Lebensmittel/Belag.h \
    Lebensmittel/form.h \
    Lebensmittel/groesse.h \
    Lebensmittel/Teig.h \
    Lebensmittel/Zutat.h \
    PhysicalObjects/Automat/automat.h \
    PhysicalObjects/Betrieb/betrieb.h \
    PhysicalObjects/Blech/blech.h \
    PhysicalObjects/Lager/einlagernlistener.h \
    PhysicalObjects/Lager/lager.h \
    PhysicalObjects/Laufband/laufband.h \
    PhysicalObjects/Laufband/speedlistener.h \
    PhysicalObjects/Laufband/stoplistener.h \
    PhysicalObjects/Ofen/ofen.h \
    PhysicalObjects/Ofen/temperaturelistener.h \
    PhysicalObjects/Physik/physik.h \
    PhysicalObjects/System/system.h \
    Playbooks/combinedplaybook.h \
    Playbooks/iplaybook.h \
    Playbooks/okplaybook.h \
    Playbooks/speedplaybook.h \
    Playbooks/stopplaybook.h \
    Playbooks/temperatureplaybook.h \
    Rezept/rezept.h \
    Schnittstellen/entnahmeschein.h \
    Schnittstellen/konfigurationsanweisung.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    Baeckerei.qmodel \
    lebensmittel.qmodel \
    baeckerei_gui.qmodel \
    controller.qmodel

STATECHARTS += \
    Baeckerei.scxml
