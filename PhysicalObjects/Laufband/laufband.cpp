#include "PhysicalObjects/Laufband/laufband.h"

Laufband::Laufband():
    m_Geschwindigkeit(0.0),
    m_backzeitZuHochEventSender(),
    m_backzeitZuNiedrigEventSender(),
    m_speedListener(this),
    m_stopListener(this)
{

}

SpeedListener &Laufband::speedListener()
{
    return m_speedListener;
}

StopListener &Laufband::stopListener()
{
    return m_stopListener;
}

EventSender<int> &Laufband::backzeitZuHochEventSender()
{
    return m_backzeitZuHochEventSender;
}

EventSender<int> &Laufband::backzeitZuNiedrigEventSender()
{
    return m_backzeitZuNiedrigEventSender;
}

double Laufband::Geschwindigkeit() const
{
    return m_Geschwindigkeit;
}

void Laufband::setGeschwindigkeit(double Geschwindigkeit)
{
    m_Geschwindigkeit = Geschwindigkeit;
}

double Laufband::trueGeschwindigkeit() const
{
    return
          m_speedListener.is_ever_updated()
       ?  m_speedListener.get()
       :  Geschwindigkeit();
}
