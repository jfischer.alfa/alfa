#include "PhysicalObjects/Laufband/laufband.h"
#include "PhysicalObjects/Laufband/speedlistener.h"

SpeedListener::SpeedListener(Laufband *pLaufband):
    PropertyListener<double> (0.0),
    m_pLaufband(pLaufband)
{

}

void SpeedListener::onPropertyChanged(double &speed)
{
    if (speed < 0.9 * m_pLaufband -> Geschwindigkeit()) {
        m_pLaufband -> backzeitZuHochEventSender().transmit(0);
    } else if (speed > 1.1 * m_pLaufband -> Geschwindigkeit()) {
        m_pLaufband -> backzeitZuNiedrigEventSender().transmit(0);
    }
}
