#ifndef LAUFBAND_H
#define LAUFBAND_H

#include "PhysicalObjects/Laufband/speedlistener.h"
#include "PhysicalObjects/Laufband/stoplistener.h"


/**
 * @brief Das Laufband
 * @description Die Geschwindigkeit des Laufbandes kann eingestellt werden.  Sie kann sich aber im Fehlerfalle
 *    verringern oder vergroessern.  Ueber- oder unterschreitet die wirkliche Geschwindigkeit die eingestellte
 *    um 10%, dann werden Events losgesendet.
 */
class Laufband
{
public:
    Laufband();

    /**
     * @brief getter fuer EventListener, der in der Konfiguration nicht vorgesehene Geschwindigkeitsveraenderungen bewirkt
     * @return der entsprechende EventListener
     */
    SpeedListener &speedListener();

    /**
     * @brief getter fuer EventListener, der das Versagen des Laufbandes bewirkt
     * @return der entsprechende EventListener
     */
    StopListener &stopListener();

    /**
     * @brief getter fuer EventSender, der anzeigt, dass Backzeit zu hoch, weil Laufband entweder zu langsam oder kaputt
     * @return der entsprechende EventSender
     */
    EventSender<int> &backzeitZuHochEventSender();

    /**
     * @brief getter fuer EventSender, der anzeigt, dass Backzeit zu niedrig, weil Laufband zu schnell
     * @return der entsprechende EventSender
     */
    EventSender<int> &backzeitZuNiedrigEventSender();

    /**
     * @brief getter fuer eingestellte Geschwindigkeit
     * @return die eingestellte Geschwindigkeit
     */
    double Geschwindigkeit() const;

    /**
     * @brief setter fuer eingestellte Geschwindigkeit
     * @param die einzustellende Geschwindigkeit
     */
    void setGeschwindigkeit(double Geschwindigkeit);

    /**
     * @brief die wirkliche Geschwindigkeit, die im Fehlerfalle von der eingestellten abweichen kann
     * @return
     */
    double trueGeschwindigkeit() const;

private:
    double m_Geschwindigkeit;

    EventSender<int> m_backzeitZuHochEventSender;
    EventSender<int> m_backzeitZuNiedrigEventSender;

    SpeedListener m_speedListener;
    StopListener m_stopListener;

};

#endif // LAUFBAND_H
