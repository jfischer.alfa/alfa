#ifndef STOPLISTENER_H
#define STOPLISTENER_H

#include "Events/events.t"

class Laufband;

/**
 * @brief EventListener, der Ausfall des Laufbandes bewirkt
 */
class StopListener:
        public PropertyListener<bool>
{
public:
    /**
     * @brief Konstruktor fuer StopListener
     * @param *pLaufband Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit StopListener(Laufband *pLaufband);

    /**
     * @brief Implementation abstrakter Methode
     * @description Nicht direkt aufrufen!
     */
    void onPropertyChanged(bool &);

private:
    Laufband *m_pLaufband;
};

#endif // STOPLISTENER_H
