#include "PhysicalObjects/Laufband/laufband.h"
#include "PhysicalObjects/Laufband/stoplistener.h"

StopListener::StopListener(Laufband *pLaufband):
    PropertyListener<bool> (false),
    m_pLaufband(pLaufband)
{

}

void StopListener::onPropertyChanged(bool &is_stopped)
{
    if (is_stopped) {
        (m_pLaufband -> backzeitZuHochEventSender()).transmit(0);
    }
}
