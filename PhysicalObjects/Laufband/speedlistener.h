#ifndef SPEEDLISTENER_H
#define SPEEDLISTENER_H

#include "Events/events.t"

class Laufband;

/**
 * @brief EventListener, der Geschwindigkeitaenderung bewirkt.
 */
class SpeedListener:
        public PropertyListener<double>
{
public:
    /**
     * @brief Konstruktor fuer SpeedListener
     * @param *pLaufband Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit SpeedListener(Laufband *pLaufband);

    /**
     * @brief Implementation abstrakter Methode
     * @description Nicht direkt aufrufen!
     */
    void onPropertyChanged(double &);

private:
    Laufband *m_pLaufband;
};

#endif // SPEEDLISTENER_H
