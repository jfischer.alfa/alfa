#ifndef PHYSIK_H
#define PHYSIK_H

#include <tuple>

#include "Events/delayeventlistener.h"
#include "Events/events.t"
#include "Playbooks/iplaybook.h"

class Physik
{
public:
    Physik(DelayEventListener *pDel);

    void play(IPlaybook *);

    PropertySender<double> &laufbandSpeed();
    PropertySender<bool> &laufbandStop();
    PropertySender<double> &ofenTemperature();

    EventSender<int> &automatStop();
    EventSender<::std::tuple<int, double>> &einlagern();

private:
    PropertySender<double> m_laufbandSpeed;
    PropertySender<bool> m_laufbandStop;
    PropertySender<double> m_ofenTemperature;

    EventSender<int> m_automatStop;
    EventSender<::std::tuple<int, double>> m_einlagern;

    DelayEventListener *m_pDel;
};

#endif // PHYSIK_H
