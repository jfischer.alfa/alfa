#include "PhysicalObjects/Physik/physik.h"

Physik::Physik(DelayEventListener *pDel):
    m_laufbandSpeed(0.0),
    m_laufbandStop(false),
    m_ofenTemperature(0.0),
    m_automatStop(),
    m_einlagern(),
    m_pDel(pDel)
{

}

void Physik::play(IPlaybook *pPlaybook)
{
    automatStop().addListener(m_pDel -> createEventListener());
    pPlaybook -> play(this);
    automatStop().transmit(0);
    automatStop().reset();
}

PropertySender<double> &Physik::laufbandSpeed()
{
    return m_laufbandSpeed;
}

PropertySender<bool> &Physik::laufbandStop()
{
    return m_laufbandStop;
}

PropertySender<double> &Physik::ofenTemperature()
{
    return m_ofenTemperature;
}

EventSender<int> &Physik::automatStop()
{
    return m_automatStop;
}

EventSender<::std::tuple<int, double> > &Physik::einlagern()
{
    return m_einlagern;
}
