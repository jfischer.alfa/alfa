#include "PhysicalObjects/Lager/einlagernlistener.h"
#include "PhysicalObjects/Lager/lager.h"

EinlagernListener::EinlagernListener(Lager *pLager):
    IEventListener< ::std::tuple<int, double> > (),
    m_pLager(pLager)
{

}

void EinlagernListener::trigger(::std::tuple<int, double> pair)
{
    m_pLager -> einlagern(
          ::std::get<0>(pair)
       ,  ::std::get<1>(pair));
}
