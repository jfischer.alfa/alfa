#ifndef EINLAGERNLISTENER_H
#define EINLAGERNLISTENER_H

#include <tuple>

#include "Events/events.t"


class Lager;

/**
 * @brief zum Mitteilen, dass Lager eine Position einlagern soll
 */
class EinlagernListener:
        public IEventListener<::std::tuple<int, double>>
{
public:
    /**
     * @brief Konstruktor fuer EinlagernListener
     * @param *pLager Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit EinlagernListener(Lager *pLager);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(::std::tuple<int, double>);

private:
    Lager *m_pLager;
};

#endif // EINLAGERNLISTENER_H
