#ifndef LAGER_H
#define LAGER_H

#include <map>

#include "Schnittstellen/entnahmeschein.h"
#include "PhysicalObjects/Lager/einlagernlistener.h"


/**
 * @brief Das Lager
 * @description Das Lager bewahrt Zutaten auf.  Es fuehrt zwei Verzeichnisse:  Zum ersten den Bestand,
 *    d.h. fuer jede Kategorie Zutat die im Lager auffindbare Menge.  Zum zweiten den verfuegbaren
 *    Bestand, d.h. die fuer jede Kategorie Zutat im Lager auffindbare Menge, die nicht reserviert wurde.
 *
 *    Um dem Lager Material zu entnehmen, benoetigen wir einen Entnahmeschein, auf dem geschrieben steht,
 *    was von welcher Kategorie Zutat in welcher Menge aus dem Lager beschafft werden soll.  Mit diesem
 *    Entnahmeschein muss die Methode process aufgerufen werden.  Sie prueft, ob der verfuegbare
 *    Bestand ausreicht, um den Auftrag ausfuehren zu koennen.  Ist das nicht der Fall, so liefert diese
 *    Methode den Wert false zurueck und sendet.  Im anderen Falle werden die entsprechende Bestaende
 *    reserviert.  Um das Material aus dem Lager zu holen, rufen wir danach die Methode finish auf.  Um die
 *    Reservierung rueckgaengig zu machen die Methode rollback.
 *
 *    Es gibt die Convenience-Methode entnehmen, die process und finish kombiniert.
 */
class Lager
{
public:
    Lager();

    /**
     * @brief Lagerbestand pruefen
     * @param entnahmeschein der Entnahmeschein
     * @return
     *    * false: der Bestand reicht nicht aus, um den Auftrag auszufuehren.  Die Daten des Objekts bleiben unveraendert.
     *    * true:  die im Entnahmeschein vermerkten Mengen werden reserviert.  Jetzt kann man entweder die Methode
     *         die Methode rollback aufrufen, um die Reserviertung rueckgaengig zu machen oder die Methode finish,
     *         um das Material aus dem Lager herauszuschaffen
     */
    bool process(const Entnahmeschein &entnahmeschein);
    /**
     * @brief Rueckgaengigmachen der durch den Entnahmeschein bewirkten Reservierungen
     * @param entnahmeschein
     */
    void rollback(const Entnahmeschein &entnahmeschein);

    /**
     * @brief holt das Material vom Lager ab
     * @description die Vermerke in den Objekten ueber die Reservierungen und den Bestand werden entsprechend angepasst.
     * @param entnahmeschein der Entnahmeschein
     */
    void finish(const Entnahmeschein &entnahmeschein);

    /**
     * @brief einlagern von Material in das Lager
     * @param zutat welche Zutat?
     * @param menge in welcher Menge?
     */
    void einlagern(int zutat, double menge);

    /**
     * @brief fasst die Methoden process und finish in einer Methode zusammen
     * @param entnahmeschein der Entnahmeschein
     * @return
     *    * false: der Bestand reicht nicht aus, um den Auftrag auszufuehren.  Die Daten des Objekts bleiben unveraendert.
     *    * true: die im Entnahmeschein vermerkten Mengen werden vom Lager abgeholt.  Die Daten im Objekt werden entsprechend
     *         angepasst.
     */
    bool entnehmen(const Entnahmeschein &entnahmeschein);

    /**
     * @brief getter fuer EventListener, der die Einlagerung von Material bewirkt
     * @return der EventListener
     */
    EinlagernListener &einlagernListener();

private:
    ::std::map<int, double> m_bestand;
    ::std::map<int, double> m_verfuegbar;

    EinlagernListener m_einlagernListener;

};

#endif // LAGER_H
