#include "PhysicalObjects/Lager/lager.h"

Lager::Lager():
    m_bestand(),
    m_verfuegbar(),
    m_einlagernListener(this)
{

}

bool Lager::process(const Entnahmeschein &entnahmeschein)
{
    bool retval =  true;
    for (
         ::std::map<int, double>::const_iterator it =  entnahmeschein.backing().begin()
         ;  it != entnahmeschein.backing().end()
         ;  ++it ) {

        // is_assortment_contains_it vermerkt, ob die Kategorien, die den im Entnahmeschein vermerkten Artikeln angehoeren,
        // dem Lager bekannt sind.
        bool is_assortment_contains_it =  false;
        for (
             ::std::map<int, double>::iterator jt =  m_verfuegbar.begin()
             ;  jt != m_verfuegbar.end()
             ;  ++jt ) {
            if (it -> first == jt -> first) {

                // Artikelkategorie ist dem Lager bekannt
                is_assortment_contains_it =  true;
                if (jt -> second < it -> second) {

                    // Lager enthaelt vom im Entnahmeschein beantragten Artikel nicht genuegend.
                    retval =  false;
                }
                break;
            }
        }
        if (!is_assortment_contains_it) retval =  false;
    }

    // falls alle Zutaten gefunden wurde, werden fuer sie Reservierungen vermerkt.
    if (retval) {
        for (
             ::std::map<int, double>::const_iterator it =  entnahmeschein.backing().begin()
             ;  it != entnahmeschein.backing().end()
             ;  ++it ) {
            m_verfuegbar.at(it -> first) -= it -> second;
        }

    }

    return retval;

}

void Lager::rollback(const Entnahmeschein &entnahmeschein)
{
    for (
         ::std::map<int, double>::const_iterator it =  entnahmeschein.backing().begin()
         ;  it != entnahmeschein.backing().end()
         ;  ++it ) {
        m_verfuegbar.at(it -> first) += it -> second;
    }
}

void Lager::finish(const Entnahmeschein &entnahmeschein)
{
    for (
         ::std::map<int, double>::const_iterator it =  entnahmeschein.backing().begin()
         ;  it != entnahmeschein.backing().end()
         ;  ++it ) {
        m_bestand.at(it -> first) -= it -> second;
    }
}

void Lager::einlagern(int zutat, double menge)
{
    // is_assortment_contains_it vermerkt, ob zutat als Kategorie dem Lager bekannt ist
    bool is_assortment_contains_it =  false;
    for (
         ::std::map<int, double>::iterator it =  m_bestand.begin()
         ;  it != m_bestand.end()
         ;  ++it ) {
        if (it -> first == zutat) {
            m_bestand[it -> first] += menge;
            m_verfuegbar[it -> first] += menge;
            is_assortment_contains_it =  true;
            break;
        }
    }

    // falls zutat dem Lager nicht bekannt ist, wird die Kategorie neu registriert und ihm menge zugeordnet
    if (!is_assortment_contains_it) {
        m_bestand[zutat] =  menge;
        m_verfuegbar[zutat] =  menge;
    }
}

bool Lager::entnehmen(const Entnahmeschein &entnahmeschein)
{
    bool retval =  process(entnahmeschein);
    if (retval) {
        finish(entnahmeschein);
    }

    return retval;
}

EinlagernListener &Lager::einlagernListener()
{
    return m_einlagernListener;
}

