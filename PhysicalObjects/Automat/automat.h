#ifndef AUTOMAT_H
#define AUTOMAT_H

#include "PhysicalObjects/Blech/blech.h"
#include "PhysicalObjects/Laufband/laufband.h"
#include "PhysicalObjects/Ofen/ofen.h"
#include "PhysicalObjects/Physik/physik.h"
#include "Playbooks/iplaybook.h"
#include "Schnittstellen/konfigurationsanweisung.h"

/**
 * @brief Die Backstrasse, bestehend aus Blech, Laufband und Ofen
 */
class Automat
{
public:
    const double Bandlaenge =  12.0;

    Automat(Physik *pPhysik);

    /**
     * @brief konfiguriert die Backstrasse
     * @param k Konfigurationsanweisung
     */
    void configure(const Konfigurationsanweisung &k);

    /**
     * @brief lass die Backstrasse laufen
     * @param playbook sogenanntes Playbook, welches Zwischenfaelle simuliert
     */
    void backen(IPlaybook &playbook);

    /**
     * @brief getter fuer das Blecih
     * @return das Blech-Objekt
     */
    Blech &blech();

    /**
     * @brief getter fuer das Laufband
     * @return das Laufband-Objekt
     */
    Laufband &laufband();

    /**
     * @brief getter fuer den Ofen
     * @return das Ofen-Objekt
     */
    Ofen &ofen();

private:
    Blech m_blech;
    Laufband m_laufband;
    Ofen m_ofen;

    Physik *m_pPhysik;
};

#endif // AUTOMAT_H
