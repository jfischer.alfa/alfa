#include "PhysicalObjects/Automat/automat.h"

Automat::Automat(Physik *pPhysik):
    m_blech(),
    m_laufband(),
    m_ofen(),
    m_pPhysik(pPhysik)
{

}

void Automat::configure(const Konfigurationsanweisung &k)
{
    laufband().setGeschwindigkeit(Automat::Bandlaenge / k.backzeit());
    ofen().setTemperatur(k.ofenTemperatur());
}

void Automat::backen(IPlaybook &playbook)
{
    m_pPhysik -> play(&playbook);
}

Ofen &Automat::ofen()
{
    return m_ofen;
}

Laufband &Automat::laufband()
{
    return m_laufband;
}

Blech &Automat::blech()
{
    return m_blech;
}
