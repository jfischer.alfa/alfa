#include "PhysicalObjects/Betrieb/betrieb.h"

Betrieb::Betrieb(Physik *pPhysik):
    m_automat(pPhysik),
    m_lager()
{

}

Automat &Betrieb::automat()
{
    return m_automat;
}

Lager &Betrieb::lager()
{
    return m_lager;
}
