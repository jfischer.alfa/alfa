#ifndef BETRIEB_H
#define BETRIEB_H

#include "PhysicalObjects/Automat/automat.h"
#include "PhysicalObjects/Lager/lager.h"
#include "PhysicalObjects/Physik/physik.h"

/**
 * @brief Betrieb, bestehend aus Zutaten-Lager und Automat
 */
class Betrieb
{
public:
    Betrieb(Physik *pPhysik);

    /**
     * @brief getter fuer Automat
     * @return das Automat-Objekt
     */
    Automat &automat();

    /**
     * @brief getter fuer lager
     * @return das Lager-Objekt
     */
    Lager &lager();

private:
    Automat m_automat;
    Lager m_lager;
};

#endif // BETRIEB_H
