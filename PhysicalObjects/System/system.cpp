#include "system.h"

System::System():
    m_betrieb(&m_physik),
    m_physik(&m_del),
    m_del()
{

}

void System::open()
{
    physik().laufbandStop().addListener(&betrieb().automat().laufband().stopListener());
    physik().laufbandSpeed().addListener(&betrieb().automat().laufband().speedListener());
    physik().ofenTemperature().addListener(&betrieb().automat().ofen().temperatureListener());

    physik().einlagern().addListener(&betrieb().lager().einlagernListener());
}

void System::reset()
{
    betrieb().automat().laufband().speedListener().reset() ;
    betrieb().automat().ofen().temperatureListener().reset();
}

Betrieb &System::betrieb()
{
    return m_betrieb;
}

Physik &System::physik()
{
    return m_physik;
}

DelayEventListener &System::del()
{
    return m_del;
}
