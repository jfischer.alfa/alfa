#ifndef SYSTEM_H
#define SYSTEM_H

#include "Events/delayeventlistener.h"
#include "PhysicalObjects/Betrieb/betrieb.h"
#include "PhysicalObjects/Physik/physik.h"

class System
{
public:
    System();

    /**
     * @brief macht System betriebsbereit
     */
    void open();

    /**
     * @brief setzt System zurueck und oeffnet es wieder
     * @description ist nach jedem Durchlauf eines Auftrages notwendig.
     *    Insbesondere in Fehlerzustaenden wird damit die Reparatur simuliert.
     */
    void reset();

    /**
     * @brief getter fuer betrieb
     * @return
     */
    Betrieb &betrieb();

    /**
     * @brief getter fuer physik
     * @return
     */
    Physik &physik();

    /**
     * @brief getter fuer eventListener zum Abmelden
     * @return
     */
    DelayEventListener &del();

private:
    Betrieb m_betrieb;
    Physik m_physik;

    DelayEventListener m_del;
};

#endif // SYSTEM_H
