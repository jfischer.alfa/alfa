#ifndef TEMPERATURELISTENER_H
#define TEMPERATURELISTENER_H

#include "Events/events.t"

class Ofen;

/**
 * @brief EventListener, der Veraenderung der Ofentemperatur bewirkt
 */
class TemperatureListener:
        public PropertyListener<double>
{
public:
    /**
     * @brief Konstruktor fuer TemperatureListener
     * @param *pLager Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit TemperatureListener(Ofen *pOfen);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void onPropertyChanged(double &);

private:
    Ofen *m_pOfen;
};

#endif // TEMPERATURELISTENER_H
