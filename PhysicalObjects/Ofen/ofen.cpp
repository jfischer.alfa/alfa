#include "PhysicalObjects/Ofen/ofen.h"

Ofen::Ofen():
    m_temperatur(0.0),
    m_ofenZuHeissEventSender(),
    m_ofenZuKaltEventSender(),
    m_temperatureListener(this)
{

}

TemperatureListener &Ofen::temperatureListener()
{
    return m_temperatureListener;
}

EventSender<int> &Ofen::ofenZuHeissEventSender()
{
    return m_ofenZuHeissEventSender;
}

EventSender<int> &Ofen::ofenZuKaltEventSender()
{
    return m_ofenZuKaltEventSender;
}

double Ofen::temperatur() const
{
    return m_temperatur;
}

void Ofen::setTemperatur(double temperatur)
{
    m_temperatur = temperatur;
}

double Ofen::trueTemperatur() const
{
    return
          m_temperatureListener.is_ever_updated()
       ?  m_temperatureListener.get()
       :  temperatur();
}
