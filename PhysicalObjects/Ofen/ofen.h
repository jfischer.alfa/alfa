#ifndef OFEN_H
#define OFEN_H

#include "PhysicalObjects/Ofen/temperaturelistener.h"


/**
 * @brief Der Ofen
 * @description Die Temperatur des Ofens kann eingestellt werden.  Im Fehlerfalle kann die wirkliche Temperatur aber
 *    von der eingestellten abweichen.  Weicht die wirkliche Temperatur mehr als 10% von der eingestellten Temperatur
 *    ab, werden entsprechende Events abgesandt.
 */
class Ofen
{
public:
    Ofen();

    /**
     * @brief getter fuer EventListener, der in der Konfiguration nicht vorgesehene Temperaturveraenderungen bewirkt
     * @return der entsprechende EventListener
     */
    TemperatureListener &temperatureListener();

    /**
     * @brief getter fuer EventSender, der Events sendet, falls Temperatur zu hoch
     * @return der entsprechende EventSender
     */
    EventSender<int> &ofenZuHeissEventSender();

    /**
     * @brief getter fuer EventSender, der Events sendet, falls Temperatur zu niedrig
     * @return der entsprechende EventSender
     */
    EventSender<int> &ofenZuKaltEventSender();

    /**
     * @brief getter fuer eingestellte Temperatur
     * @return die eingestellte Temperatur
     */
    double temperatur() const;

    /**
     * @brief setter zum Einstellen der Temperatur
     * @param die einzustellende Temperatur
     */
    void setTemperatur(double temperatur);

    /**
     * @brief die wirkliche Temperatur, die von der eingestellten abweichen kann
     * @return die wirkliche Temperatur
     */
    double trueTemperatur() const;

private:
    double m_temperatur;

    EventSender<int> m_ofenZuHeissEventSender;
    EventSender<int> m_ofenZuKaltEventSender;

    TemperatureListener m_temperatureListener;

};

#endif // OFEN_H
