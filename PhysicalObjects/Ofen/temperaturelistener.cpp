#include "PhysicalObjects/Ofen/ofen.h"
#include "PhysicalObjects/Ofen/temperaturelistener.h"

TemperatureListener::TemperatureListener(Ofen *pOfen):
    PropertyListener<double> (0.0),
    m_pOfen(pOfen)
{

}

void TemperatureListener::onPropertyChanged(double &temperature)
{
    if (temperature < 0.9 * m_pOfen -> temperatur()) {
        m_pOfen -> ofenZuKaltEventSender().transmit(0);
    } else if (temperature > 1.1 * m_pOfen -> temperatur()) {
        m_pOfen -> ofenZuHeissEventSender().transmit(0);
    }
}
