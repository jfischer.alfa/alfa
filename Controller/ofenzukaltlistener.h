#ifndef OFENZUKALTLISTENER_H
#define OFENZUKALTLISTENER_H

#include "Events/events.t"

class BaeckereiController;

/**
 * @brief wird zum Mitteilen verwendet, dass Ofen zu kalt sei.
 */
class OfenZuKaltListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer OfenZuKaltListener
     * @param *pController Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit OfenZuKaltListener(BaeckereiController *ptrController);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(int);

private:
    BaeckereiController *ptrController;
};

#endif // OFENZUKALTLISTENER_H
