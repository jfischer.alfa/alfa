#include "baeckereicontroller.h"
#include <QDebug>
#include "Playbooks/okplaybook.h"
#include "Playbooks/temperatureplaybook.h"
#include "Playbooks/speedplaybook.h"
#include <thread>
#include <math.h>


void BaeckereiController::automatFertig()
{
    qDebug("Plätzchen gebacken und verziert!!!!!!!");
    // Auftrag loggen
    Logger::loggeAuftrag(auftrag);
    emit (backenZuEnde());
    auftragsCounter++;
}

BaeckereiController::BaeckereiController(QWidget *ptrUi) : QObject(ptrUi),
    auftrag(Auftrag(-1,-1,-1,-1,-1,-1)),
    ptrSystem(nullptr),
    ptrBaeckermeister{nullptr},
    ptrBacktimer{nullptr},
    automatStopListener{this},
    backzeitZuHochListener{this},
    backzeitZuNiedrigListener{this},
    ofenZuKaltListener{this},
    ofenZuHeissListener{this}
{
    this->ptrSystem =  new System();
    this->ptrBaeckermeister = new Baeckermeister();
    this->ptrBacktimer = new QTimer();
    this->ptrBacktimer->setSingleShot(true);

    // Listener ans System
    ptrSystem->open();
    ptrSystem->betrieb().automat().laufband().backzeitZuHochEventSender().addListener(&this->backzeitZuHochListener);
    ptrSystem->betrieb().automat().laufband().backzeitZuNiedrigEventSender().addListener(&this->backzeitZuNiedrigListener);
    ptrSystem->betrieb().automat().ofen().ofenZuHeissEventSender().addListener(&this->ofenZuHeissListener);
    ptrSystem->betrieb().automat().ofen().ofenZuKaltEventSender().addListener(&this->ofenZuKaltListener);

    lagerFuellen();
    QObject::connect(ptrBacktimer,SIGNAL(timeout()),this,SLOT(automatFertig()));
}

BaeckereiController::~BaeckereiController()
{
    delete ptrSystem;
    delete ptrBaeckermeister;
    delete ptrBacktimer;
    delete ptrPlaybook;
}

void BaeckereiController::lagerFuellen()
{

    // Map für Grundbestand
    std::map<int,double> grundbestand;
    grundbestand.insert(pair<int,double>(Zutat::eier,100000));
    grundbestand.insert(pair<int,double>(Zutat::mehl,100000));
    grundbestand.insert(pair<int,double>(Zutat::zucker,100000));
    grundbestand.insert(pair<int,double>(Zutat::zuckerguss,100000));
    grundbestand.insert(pair<int,double>(Zutat::streusel,100000));
    grundbestand.insert(pair<int,double>(Zutat::milch,100000));
    grundbestand.insert(pair<int,double>(Zutat::nuesse,100000));
    grundbestand.insert(pair<int,double>(Zutat::backpulver,100000));
    grundbestand.insert(pair<int,double>(Zutat::schokoguss,100000));

    // Grundbestand einlagern
    for(std::pair<int,double> p : grundbestand){
        this->ptrSystem->betrieb().lager().einlagern(p.first,p.second);
    }
    qDebug("Lager wurde aufgefuellt");
}

IPlaybook* BaeckereiController::waehlePlaybook()
{
    int index = std::rand() % 5;

    switch (index){
    case 0:
        return new TemperaturePlaybook();
    case 1:
        return new SpeedPlaybook();
    default:
        return new OKPlaybook();
    }
}


void BaeckereiController::ausfuehren(int teigsorte, int form, int stueckzahl, int belag, int plaetzchengroesse)
{

    auftrag = Auftrag (auftragsCounter,teigsorte,form,stueckzahl,plaetzchengroesse,belag);

    // Auftragsnummer fuer UI
    emit (auftragsNummer(auftrag.getAuftragsNummer()));

    // Rezept anhand des Auftrags vom Baeckermeister holen
    Rezept rezept{this->ptrBaeckermeister->getRezept(auftrag)};

    // Entnahmeschein anhand der Zutatenliste erstellen
    Entnahmeschein entnahmeSchein{rezept.getZutatenliste()};

    // Anfrage im Lager mit Entnahmeschein
    bool verfuegbar = ptrSystem->betrieb().lager().entnehmen(entnahmeSchein);

    if(verfuegbar){

        // Konfigurationsanweisung zum Backen vom Rezept
        Konfigurationsanweisung konfig{rezept.getBackZeit(),rezept.getBackTemperatur()};

        // Einstellen der Maschine
        ptrSystem->betrieb().automat().configure(konfig);

        // Starten des Backtimers
        this->ptrBacktimer->start(static_cast<int>(rezept.getBackZeit())*1000);

        // Waehlt das Playbook
        this->ptrPlaybook = waehlePlaybook();

        // Backprozess gestartet
        ptrSystem->betrieb().automat().backen(*ptrPlaybook);

        // Ruecksetzen der Listener
        ptrSystem->reset();

    } else {
        qDebug("Zutaten nicht auf lager");
        emit zutatNichtAufLagerSignal();
    }

}

void BaeckereiController::fehler()
{
    Logger::logFehler(this->ptrPlaybook);
}


