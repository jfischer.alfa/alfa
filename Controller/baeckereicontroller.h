#ifndef BAECKEREICONTROLLER_H
#define BAECKEREICONTROLLER_H
#include <QWidget>
#include <QObject>
#include <QTimer>

#include "Auftrag/logger.h"
#include "Lebensmittel/Baeckermeister.h"
#include "PhysicalObjects/System/system.h"

#include "Controller/automatstoplistener.h"
#include "Controller/backzeitzuhochlistener.h"
#include "Controller/backzeitzuniedriglistener.h"
#include "Controller/ofenzuheisslistener.h"
#include "Controller/ofenzukaltlistener.h"


/**
 * @brief Baeckereicontroller
 * @description Enthält alle Modell-Komponenten, steuert den
 * Ablauf und kommuniziert mit der Qt-UI (MainWindow.cpp).
 */
class BaeckereiController : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief Initialisierung der Komponenten, nimmt optional Zeiger auf UI
     */
    explicit BaeckereiController(QWidget *ptrUi = nullptr);
    ~BaeckereiController();


    /**
     * @brief Enthält den Prozessablauf.
     * @description Aus den Parametern wird ein Auftrag erzeugt, anschließend
     * Ablauf und kommuniziert mit der Qt-UI (MainWindow.cpp).
     */
    void ausfuehren(int teigsorte, int form, int stueckzahl, int belag, int plaetzchengroesse);

    /**
     * @brief Bei Fehler wird Auftrag abgebrochen.
     */
    void fehler();

public slots:
    /**
     * @brief Fuellt das Lager.
     */
    void lagerFuellen();

private:

    QWidget* ptrUi;

    // System & Komponenten
    Auftrag auftrag;
    int auftragsCounter{1};
    System* ptrSystem;
    Baeckermeister* ptrBaeckermeister;
    QTimer* ptrBacktimer;
    IPlaybook* ptrPlaybook;

    /// Zustaende des Automaten
    AutomatStopListener automatStopListener;
    BackzeitZuHochListener backzeitZuHochListener;
    BackzeitZuNiedrigListener backzeitZuNiedrigListener;
    OfenZuKaltListener ofenZuKaltListener;
    OfenZuHeissListener ofenZuHeissListener;


    /**
     * @brief Waehlt das Ereignis-Playbook
     * @description Das Playbook bestimmt den Erfolg des Backprozesses. Weitere Infos siehe @class Playbook
     */
    IPlaybook* waehlePlaybook();

// Signals für MainWindow.cpp
signals:


    /**
     * @brief Übergibt Auftragsnummer
     */
    void auftragsNummer(int);

    /**
     * @brief Signal wenn Backen zu Ende
     */
    void backenZuEnde();

    /**
     * @brief Signal wenn Backzeit zu hoch
     */
    void backzeitZuHochSignal();

    /**
     * @brief Signal wenn Backzeit zu niedrig
     */
    void backzeitZuNiedrigSignal();

    /**
     * @brief Signal wenn Temperatur zu hoch
     */
    void ofenZuHeissSignal();


    /**
     * @brief Signal wenn Temperatur zu niedrig
     */
    void ofenZuKaltSignal();

    /**
     * @brief Signal wenn Zutaten nicht auf Lager
     */
    void zutatNichtAufLagerSignal();

private slots:
    void automatFertig();



};

#endif // BAECKEREICONTROLLER_H
