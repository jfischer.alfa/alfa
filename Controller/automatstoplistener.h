#ifndef AUTOMATSTOPLISTENER_H
#define AUTOMATSTOPLISTENER_H

#include "Events/events.t"

class BaeckereiController;

/**
 * @brief Wird verwendet, damit Automat ihr Arbeitsende mitteilen kann
 */
class AutomatStopListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer AutomatStopListener
     * @param *pController Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit AutomatStopListener(BaeckereiController *pController);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(int);

private:
    BaeckereiController *ptrController;

};

#endif // AUTOMATSTOPLISTENER_H
