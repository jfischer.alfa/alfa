#ifndef BACKZEITZUHOCHLISTENER_H
#define BACKZEITZUHOCHLISTENER_H

#include "Events/events.t"

class BaeckereiController;

/**
 * @brief wird zum Mitteilen verwendet, dass Backzeit zu hoch sei.
 */
class BackzeitZuHochListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer BackzeitZuHochListener
     * @param *pController Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit BackzeitZuHochListener(BaeckereiController *pController);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(int);

private:
    BaeckereiController *ptrController;

};

#endif // BACKZEITZUHOCHLISTENER_H
