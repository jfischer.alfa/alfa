#ifndef OFENZUHEISSLISTENER_H
#define OFENZUHEISSLISTENER_H

#include "Events/events.t"


class BaeckereiController;

/**
 * @brief wird zum Mitteilen verwendet, dass Ofen zu heiss sei.
 */
class OfenZuHeissListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer OfenZuHeissListener
     * @param *pController Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit OfenZuHeissListener(BaeckereiController *ptrController);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(int);

private:
    BaeckereiController *ptrController;

};

#endif // OFENZUHEISSLISTENER_H
