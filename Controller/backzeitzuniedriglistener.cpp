#include "Controller/backzeitzuniedriglistener.h"
#include "Controller/baeckereicontroller.h"

BackzeitZuNiedrigListener::BackzeitZuNiedrigListener(BaeckereiController *pBackend):
    IEventListener<int> (),
    ptrController{pBackend}
{
}

void BackzeitZuNiedrigListener::trigger(int)
{
        ptrController->fehler();
    emit(ptrController -> backzeitZuNiedrigSignal());
       qDebug("backzeitZuNiedrigSignal");
}
