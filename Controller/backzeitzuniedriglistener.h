#ifndef BACKZEITZUNIEDRIGLISTENER_H
#define BACKZEITZUNIEDRIGLISTENER_H

#include "Events/events.t"

class BaeckereiController;

/**
 * @brief wird zum Mitteilen verwendet, dass Backzeit zu niedrig sei.
 */
class BackzeitZuNiedrigListener:
        public IEventListener<int>
{
public:
    /**
     * @brief Konstruktor fuer BackzeitZuNiedrigListener
     * @param *pController Klasse, dessen Bestandteil dieser EventListener ist
     */
    explicit BackzeitZuNiedrigListener(BaeckereiController *ptrController);

    /**
     * @brief Implementation der entsprechenden Methode der Oberklasse
     * @description Nicht direkt aufrufen!
     */
    void trigger(int);

private:
    BaeckereiController *ptrController;

};

#endif // BACKZEITZUNIEDRIGLISTENER_H
