#include "Controller/baeckereicontroller.h"
#include "Controller/ofenzukaltlistener.h"

OfenZuKaltListener::OfenZuKaltListener(BaeckereiController *pcontrol):
    IEventListener<int> (),
    ptrController(pcontrol)
{

}

void OfenZuKaltListener::trigger(int)
{
        ptrController->fehler();
    emit(ptrController -> ofenZuKaltSignal());
    qDebug("ofenZuHeissSignal");
}
