#include "Controller/backzeitzuhochlistener.h"
#include "Controller/baeckereicontroller.h"

BackzeitZuHochListener::BackzeitZuHochListener(BaeckereiController *pController):
    IEventListener<int> (),
    ptrController{pController}
{

}

void BackzeitZuHochListener::trigger(int)
{
        ptrController->fehler();
    emit(ptrController -> backzeitZuHochSignal());
        qDebug("backzeitZuHochSignal");
}
