#include "Controller/ofenzuheisslistener.h"
#include "Controller/baeckereicontroller.h"

OfenZuHeissListener::OfenZuHeissListener(BaeckereiController *pcontroller):
    IEventListener<int> (),
    ptrController{pcontroller}
{

}

void OfenZuHeissListener::trigger(int)
{
    ptrController->fehler();
    emit(ptrController -> ofenZuHeissSignal());
    qDebug("ofenZuHeissSignal");
}
