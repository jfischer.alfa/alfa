#include "auftrag.h"


Auftrag::Auftrag(int auftragsnummer, int teigsorte, int form, int stueckzahl, int groesse, int belag)
{
    this->auftragsNummer = auftragsnummer;
	this->stueckzahl = stueckzahl;
    this->plaetzchengroesse = groesse;
	this->teigsorte = teigsorte;
	this->belag = belag;
    this->form = form;
}

Auftrag::~Auftrag()
{
}

int Auftrag::getAuftragsNummer()
{
    return this->auftragsNummer;
}

int Auftrag::getStueckzahl()
{
	return this->stueckzahl;
}

int Auftrag::getTeigsorte()
{
	return this->teigsorte;
}

int Auftrag::getPlaetzchengroesse()
{
	return this->plaetzchengroesse;
}

int Auftrag::getBelag()
{
    return this->belag;
}

int Auftrag::getForm()
{
    return this->form;

}
