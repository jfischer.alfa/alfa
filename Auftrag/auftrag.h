#pragma once

/**
 * @brief Auftrag
 * @description Enthält alle Daten die durch Eingabe in der UI erfasst werden
 * zzgl. einer vom System generierten Auftragsnummer
 */
class Auftrag
{

public: 

    /**
     * @brief Kontruktor für Auftrag
     * @param Auftragsnummer
     * @param Teigsorte
     * @param Plaetzchenform
     * @param Stueckzahl
     * @param Plaetzchengroesse
     * @param Belag
     */
    Auftrag(int auftragsnummer, int teigsorte, int form, int stueckzahl, int groesse, int belag);

    /**
     * @brief *deleted* DefaultKonstruktor
     * @description Zur Erzeugung eines Auftrags müssen alle relevanten Daten verfügbar sein.
     */
    Auftrag() = delete;
    Auftrag(const Auftrag&);
	~Auftrag();

    int getAuftragsNummer();
	int getStueckzahl();
	int getTeigsorte();
	int getPlaetzchengroesse();
	int getBelag();
    int getForm();

private:
    int auftragsNummer;
	int stueckzahl;
	int teigsorte;
	int plaetzchengroesse;
	int belag;
    int form;
};

