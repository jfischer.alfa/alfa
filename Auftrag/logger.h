#pragma once

#include <iostream>
#include <fstream>

#include "auftrag.h"
#include "Playbooks/iplaybook.h"

/**
 * @brief Logger
 * @description Schreibt Logdaten bei Erstellung eines Auftrags.
 */
class Logger
{

public: 
	
    /**
     * @brief Hinzufügen eines Logeintrags
     * @param Zu loggender Auftrag
     */
    static void loggeAuftrag(Auftrag&);

    /**
     * @brief Fügt Fehler zum Logeintrag des Auftrags
     * @param Zu loggender Fehler (Playbook)
     */
    static void logFehler(IPlaybook*);


    /**
     * @brief Auslesen und Rueckgabe der Logdatei
     */
	 static std::string printLog();

     /**
      * @brief Moeglichkeit die Logdateien zu loeschen
      */
	 static void clearLogs();

private:

    /// Pfad zur Logdatei
    static constexpr const char* auftragsLogFile = "logs_auftraege.csv";
        static constexpr const char* fehlerLogFile = "logs_fehler.csv";
};

