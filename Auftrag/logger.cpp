#include "logger.h"
#include <chrono>
#include "Lebensmittel/Baeckermeister.h"
#include "Playbooks/okplaybook.h"
#include "Playbooks/temperatureplaybook.h"
#include "Playbooks/speedplaybook.h"


void Logger::loggeAuftrag(Auftrag& a)
{
    std::fstream fs;
    fs.open(auftragsLogFile,std::ios::app);
    if (fs.is_open()) {
        std::time_t result = std::time(nullptr);
        fs << std::asctime(std::localtime(&result))
           << ";"
           << a.getAuftragsNummer()
           << ";"
           << a.getStueckzahl()
           << ";"
           << Teig::getString(a.getTeigsorte())
           << ";"
           << Groesse::getString(a.getPlaetzchengroesse())
           << ";"
           << Belag::getString(a.getBelag())
           << ";"
           << Form::getString(a.getForm())
           << ";"
           << std::endl;

        fs.close();
    }
    else {
        std::cout << "Failed to log." << std::endl;
    }
}

void Logger::logFehler(IPlaybook *playbook)
{

    TemperaturePlaybook* tempPlaybook = dynamic_cast<TemperaturePlaybook*>(playbook);
    SpeedPlaybook* speedPlay = dynamic_cast<SpeedPlaybook*>(playbook);

    std::fstream fs;
    fs.open(fehlerLogFile,std::ios::app);
    if (fs.is_open()) {

        std::time_t result = std::time(nullptr);
        fs << std::asctime(std::localtime(&result))
           << ";";

        if(tempPlaybook != nullptr){
            fs << "TEMPFEHLER;";

        }
        else if(speedPlay != nullptr){
            fs << "SPEEDFEHLER;";
        }

        fs << std::endl;

        fs.close();
    }
    else {
        std::cout << "Failed to log." << std::endl;
    }
}

std::string Logger::printLog()
{
    std::fstream fs;
    std::string logs{""};

    fs.open(fehlerLogFile, std::ios::in | std::ios::binary);

    if (!fs.is_open()) {
        std::cout << "failed to access logfile." << fehlerLogFile << std::endl;
    }
    else {
        char c;
        while (fs.get(c)) {
            logs += c;
        }
        fs.close();
        std::cout << "LOGS DUMP: " << std::endl << logs << std::endl;
    }
    return logs;
}

void Logger::clearLogs()
{
    std::remove(auftragsLogFile);
    std::remove(fehlerLogFile);
}
