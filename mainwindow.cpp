#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{ this->baecker = new BaeckereiController();
    fensterdesign();
    //Teig
    connect(this->pushButton,SIGNAL( clicked()),this,SLOT(schreibenB()));
    connect(this->pushButton1,SIGNAL( clicked()),this,SLOT(schreibenB1()));
    connect(this->pushButton2,SIGNAL( clicked()),this,SLOT(schreibenB2()));
    //Format
    connect(this->pushButton3,SIGNAL( clicked()),this,SLOT(schreibenB3()));
    connect(this->pushButton4,SIGNAL( clicked()),this,SLOT(schreibenB4()));
    connect(this->pushButton5,SIGNAL( clicked()),this,SLOT(schreibenB5()));
    //Groesse
    connect(this->pushButton6,SIGNAL( clicked()),this,SLOT(schreibenB6()));
    connect(this->pushButton7,SIGNAL( clicked()),this,SLOT(schreibenB7()));
    connect(this->pushButton8,SIGNAL( clicked()),this,SLOT(schreibenB8()));
    //Stueckzahl
    connect(this->pushButton9,SIGNAL( clicked()),this,SLOT(schreibenB9()));
    connect(this->pushButton10,SIGNAL( clicked()),this,SLOT(schreibenB10()));
    connect(this->pushButton11,SIGNAL( clicked()),this,SLOT(schreibenB11()));
    //Belag
    connect(this->pushButton12,SIGNAL( clicked()),this,SLOT(schreibenB12()));
    connect(this->pushButton13,SIGNAL( clicked()),this,SLOT(schreibenB13()));
    connect(this->pushButton14,SIGNAL( clicked()),this,SLOT(schreibenB14()));

    //ProduktionsStart und Verarbeitung
    connect(this->pushButton15,SIGNAL( clicked()),this,SLOT(auftrag()));
    connect(baecker,SIGNAL(auftragsNummer(int)),this,SLOT(auftragsnummer(int)));
    connect(baecker,SIGNAL(backzeitZuHochSignal()),this,SLOT(backzeitZuHochSignal()));
    connect(baecker,SIGNAL(backzeitZuNiedrigSignal()),this,SLOT(backzeitZuNiedrigSignal()));
    connect(baecker,SIGNAL(ofenZuHeissSignal()),this,SLOT(ofenZuHeissSignal()));
    connect(baecker,SIGNAL(ofenZuKaltSignal()),this,SLOT(ofenZuKaltSignal()));
    connect(baecker,SIGNAL(zutatNichtAufLagerSignal()),this,SLOT(zutatNichtAufLagerSignal()));
    connect(baecker,SIGNAL(backenZuEnde()),this,SLOT(backenbeendet()));
    connect(this->pushButton16,SIGNAL( clicked()),baecker,SLOT(lagerFuellen()));
    connect(this->pushButton16,SIGNAL( clicked()),this,SLOT(lagerGefuellt()));

}

void MainWindow::fensterdesign(){
    // Titel
    this->resize(700,570);
    this->setWindowTitle("Plätzchen Fabrik Zur Oma");

    this->label= new QLabel(this);
    this->label->setGeometry(50,35,290,30);
    this->label->setText("PRODUKTIONSAUFTRAG  PLÄTZCHEN");
    QFont font;
    font.setBold(1);
    font.setPointSize(9);
    this->label->setFont(font);
    this->label->setStyleSheet("QLabel { background-color: lightyellow }");
    this->label->setAlignment(Qt::AlignCenter);


    //Auftragsstatus
    this->label11= new QLabel(this);
    this->label11->setGeometry(490,35,120,30);
    this->label11->setText("AUFTRAGSSTATUS");
    font.setBold(1);
    font.setPointSize(9);
    this->label11->setFont(font);
    this->label11->setStyleSheet("QLabel { background-color: lightyellow }");
    this->label11->setAlignment(Qt::AlignCenter);

    //Auftrag / Status
    //Auftrag
    this->label12= new QLabel(this);
    this->label12->setGeometry(490,100,120,30);
    this->label12->setText("");
    font.setPointSize(9);
    this->label12->setFont(font);
    this->label12->setStyleSheet("QLabel { background-color: lightgreen}");
    this->label12->setAlignment(Qt::AlignCenter);

    this->label13= new QLabel(this);
    this->label13->setGeometry(430,108,40,16);
    this->label13->setText("Auftrag");
    this->label13->setStyleSheet("QLabel { background-color: lightyellow }");
    font.setPointSize(7);
    this->label13->setFont(font);
    this->label13->setAlignment(Qt::AlignCenter);



    //Allgemeiner Status, orange = noch nicht gestartet/beendet,  gruen=in progress, rot=Stoerung
    this->label16= new QLabel(this);
    this->label16->setGeometry(520,200,40,30);
    this->label16->setText("");
    font.setPointSize(9);
    this->label16->setFont(font);
    this->label16->setStyleSheet("QLabel { background-color: orange }");
    this->label16->setAlignment(Qt::AlignCenter);

    this->label17= new QLabel(this);
    this->label17->setGeometry(430,208,40,16);
    this->label17->setText("Status");
    this->label17->setStyleSheet("QLabel { background-color: lightyellow }");
    font.setPointSize(7);
    this->label17->setFont(font);
    this->label17->setAlignment(Qt::AlignCenter);

    //Status Nachricht Text
    this->label18= new QLabel(this);
    this->label18->setGeometry(50,510,605,40);
    this->label18->setText("");
    this->label18->setStyleSheet("QLabel { background-color: orange }");
    font.setPointSize(8);
    font.setBold(1);
    this->label18->setFont(font);
    this->label18->setAlignment(Qt::AlignCenter);

    //Auftragserstellung
    // Teigauswahl
    this->pushButton = new QPushButton(this);
    this->pushButton->setGeometry(50,100,60,30);
    this->pushButton->setText("Blätterteig");
    this->pushButton->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->pushButton1 = new QPushButton(this);
    this->pushButton1->setGeometry(130,100,60,30);
    this->pushButton1->setText("Rührteig");
    this->pushButton1->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->pushButton2 = new QPushButton(this);
    this->pushButton2->setGeometry(210,100,60,30);
    this->pushButton2->setText("Hefeteig");
    this->pushButton2->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->label1= new QLabel(this);
    this->label1->setGeometry(300,108,40,16);
    this->label1->setText("Teig");
    this->label1->setStyleSheet("QLabel { background-color: lightyellow }");
    font.setPointSize(7);
    this->label1->setFont(font);
    this->label1->setAlignment(Qt::AlignCenter);


    //Auswahl der Plätzchenform
    this->pushButton3 = new QPushButton(this);
    this->pushButton3->setGeometry(50,150,60,30);
    this->pushButton3->setText("Herz");
    this->pushButton3->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->pushButton4 = new QPushButton(this);
    this->pushButton4->setGeometry(130,150,60,30);
    this->pushButton4->setText("Quadrat");
    this->pushButton4->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->pushButton5 = new QPushButton(this);
    this->pushButton5->setGeometry(210,150,60,30);
    this->pushButton5->setText("Kreis");
    this->pushButton5->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->label2= new QLabel(this);
    this->label2->setGeometry(300,158,40,16);
    this->label2->setText("Form");
    this->label2->setStyleSheet("QLabel { background-color: lightgreen }");
    font.setPointSize(7);
    this->label1->setFont(font);
    this->label2->setAlignment(Qt::AlignCenter);


    //Auswahl der PlätzchenGroesse
    this->pushButton6 = new QPushButton(this);
    this->pushButton6->setGeometry(50,200,60,30);
    this->pushButton6->setText("Eins");
    this->pushButton6->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->pushButton7 = new QPushButton(this);
    this->pushButton7->setGeometry(130,200,60,30);
    this->pushButton7->setText("Zwei");
    this->pushButton7->setStyleSheet("QPushButton{ background-color: lightgreen }");


    this->pushButton8 = new QPushButton(this);
    this->pushButton8->setGeometry(210,200,60,30);
    this->pushButton8->setText("Drei");
    this->pushButton8->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->label3= new QLabel(this);
    this->label3->setGeometry(300,208,40,16);
    this->label3->setText("Größe");
    this->label3->setStyleSheet("QLabel { background-color: lightyellow }");
    font.setPointSize(7);
    this->label3->setFont(font);
    this->label3->setAlignment(Qt::AlignCenter);


    //Auswahl der Stückzahl
    this->pushButton9 = new QPushButton(this);
    this->pushButton9->setGeometry(50,250,60,30);
    this->pushButton9->setText("300");
    this->pushButton9->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->pushButton10 = new QPushButton(this);
    this->pushButton10->setGeometry(130,250,60,30);
    this->pushButton10->setText("500");
    this->pushButton10->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->pushButton11 = new QPushButton(this);
    this->pushButton11->setGeometry(210,250,60,30);
    this->pushButton11->setText("1000");
    this->pushButton11->setStyleSheet("QPushButton{ background-color: lightyellow }");

    this->label4= new QLabel(this);
    this->label4->setGeometry(300,258,40,16);
    this->label4->setText("Anzahl");
    this->label4->setStyleSheet("QLabel { background-color: lightgreen }");
    font.setPointSize(7);
    this->label4->setFont(font);
    this->label4->setAlignment(Qt::AlignCenter);


    //Auswahl des Belags
    this->pushButton12 = new QPushButton(this);
    this->pushButton12->setGeometry(50,300,60,30);
    this->pushButton12->setText("Zuckerguß");
    this->pushButton12->setStyleSheet("QPushButton{ background-color: lightgreen }");


    this->pushButton13 = new QPushButton(this);
    this->pushButton13->setGeometry(130,300,60,30);
    this->pushButton13->setText("Schoko");
    this->pushButton13->setStyleSheet("QPushButton{ background-color: lightgreen }");


    this->pushButton14 = new QPushButton(this);
    this->pushButton14->setGeometry(210,300,60,30);
    this->pushButton14->setText("Streusel");
    this->pushButton14->setStyleSheet("QPushButton{ background-color: lightgreen }");

    this->label5= new QLabel(this);
    this->label5->setGeometry(300,308,40,16);
    this->label5->setText("Belag");
    this->label5->setStyleSheet("QLabel { background-color: lightyellow}");
    font.setPointSize(7);
    this->label5->setFont(font);
    this->label5->setAlignment(Qt::AlignCenter);



    // Auftragssummary
    // Teig
    this->label6= new QLabel(this);
    this->label6->setGeometry(50,410,80,30);
    this->label6->setStyleSheet("QLabel { background-color: lightgreen}");
    this->label6->setAlignment(Qt::AlignCenter);

    //Form
    this->label7= new QLabel(this);
    this->label7->setGeometry(140,410,80,30);
    this->label7->setStyleSheet("QLabel { background-color: lightyellow}");
    this->label7->setAlignment(Qt::AlignCenter);

    //Groesse
    this->label8= new QLabel(this);
    this->label8->setGeometry(230,410,80,30);
    this->label8->setStyleSheet("QLabel { background-color: lightgreen}");
    this->label8->setAlignment(Qt::AlignCenter);

    //Anzahl
    this->label9= new QLabel(this);
    this->label9->setGeometry(320,410,80,30);
    this->label9->setStyleSheet("QLabel { background-color: lightyellow}");
    this->label9->setAlignment(Qt::AlignCenter);

    //Belag
    this->label10= new QLabel(this);
    this->label10->setGeometry(410,410,80,30);
    this->label10->setStyleSheet("QLabel { background-color: lightgreen}");
    this->label10->setAlignment(Qt::AlignCenter);

    // Produktionsstart


    this->pushButton15 = new QPushButton(this);
    this->pushButton15->setGeometry(50,460,40,30);
    this->pushButton15->setText("START");
    font.setBold(1);
    this->pushButton15->setFont(font);
    this->pushButton15->setStyleSheet("QPushButton{ background-color: orange }");

    //Restock Button
    this->pushButton16 = new QPushButton(this);
    this->pushButton16->setGeometry(590,460,60,30);
    this->pushButton16->setText("RESTOCK");
    font.setBold(1);
    this->pushButton16->setFont(font);
    this->pushButton16->setStyleSheet("QPushButton{ background-color: orange }");


}



//Teigsorte Auftrag fuellen
void MainWindow::schreibenB(){

    l6 = this->pushButton->text();
    this->label6->setText(l6);
    this->teig = Teig::blaetterteig;

}

void MainWindow::schreibenB1(){
    l6 = this->pushButton1->text();
    this->label6->setText(l6);
    this->teig = Teig::ruehrteig;
}

void MainWindow::schreibenB2(){
    l6 = this->pushButton2->text();
    this->label6->setText(l6);
    this->teig = Teig::hefeteig;
}

//Plätzchen Form Auftrag fuellen
void MainWindow::schreibenB3(){
    l7 = this->pushButton3->text();
    this->label7->setText(l7);
    this->form = Form::herz;
}

void MainWindow::schreibenB4(){
    l7 = this->pushButton4->text();
    this->label7->setText(l7);
        this->form = Form::quadrat;
}

void MainWindow::schreibenB5(){
    l7 = this->pushButton5->text();
    this->label7->setText(l7);
    this->form = Form::kreis;
}

//Plätzchen Groesse Auftrag fuellen
void MainWindow::schreibenB6(){
    l8 = this->pushButton6->text();
    this->label8->setText(l8);
    this->groesse = Groesse::ein;
}

void MainWindow::schreibenB7(){
    l8 = this->pushButton7->text();
    this->label8->setText(l8);
    this->groesse = Groesse::zwei;
}

void MainWindow::schreibenB8(){
    l8 = this->pushButton8->text();
    this->label8->setText(l8);
    this->groesse = Groesse::drei;
}

//Plätzchen Stueckzahl Auftrag fuellen
void MainWindow::schreibenB9(){
    l9 = this->pushButton9->text();
    this->label9->setText(l9);
    this->stueckzahl=300;
}

void MainWindow::schreibenB10(){
    l9 = this->pushButton10->text();
    this->label9->setText(l9);
    this->stueckzahl=500;
}

void MainWindow::schreibenB11(){
    l9 = this->pushButton11->text();
    this->label9->setText(l9);
    this->stueckzahl=1000;

}

//Plätzchen Belag Auftrag fuellen
void MainWindow::schreibenB12(){
    l10 = this->pushButton12->text();
    this->label10->setText(l10);
    this->belag = Belag::zuckerglasur;
}

void MainWindow::schreibenB13(){
    l10 = this->pushButton13->text();
    this->label10->setText(l10);
    this->belag = Belag::amerikaner;

}

void MainWindow::schreibenB14(){
    l10 = this->pushButton14->text();
    this->label10->setText(l10);
    this->belag = Belag::zuckerstreusel;

}

//SLOTS Auftrag erzeugen und verarbeiten
void MainWindow::auftrag(){

    if (label6->text()==""||label7->text()==""||label8->text()==""||
            label9->text()==""||label10->text()=="")
    {
        this->label18->setText(" DER AUFTRAG IST UNVOLLSTÄNDIG ");
    }
    else {

        fehlerGesamt="";

        this->label18->setText(" AUFTRAG  AN  PRODUKTION  GESENDET ");
        this->label16->setStyleSheet("QLabel { background-color: lightgreen }");

        baecker->ausfuehren(this->teig,this->form,this->stueckzahl,this->belag,this->groesse);

        this->label6->setText("");
        this->label7->setText("");
        this->label8->setText("");
        this->label9->setText("");
        this->label10->setText("");

        teig=-1;
        belag=-1;
        form=-1;
        groesse=-1;
        stueckzahl=-1;
    }

}

void MainWindow::auftragsnummer(int auftragsnummer){

    this->label12->setText(QString::number(auftragsnummer));

}

void MainWindow::backenbeendet(){
    this->label18->setText(" BACKEN  BEENDET ");
    this->label16->setStyleSheet("QLabel { background-color: orange }");
}

void MainWindow::lagerGefuellt(){
    this->label18->setText(" LAGER  WURDE  BESTUECKT ");
    this->label16->setStyleSheet("QLabel { background-color: lightgreen }");
}


//SLOTS fuer die Fehlermeldungen

void MainWindow::backzeitZuHochSignal(){

     schreibeFehlerCode(" BACKZEIT  ÜBERSCHREITUNG    ");
}

void MainWindow::backzeitZuNiedrigSignal(){
     schreibeFehlerCode(" BACKZEIT  UNTERSCHREITUNG    ");
}

void MainWindow::ofenZuHeissSignal(){

    schreibeFehlerCode(" TEMPERATUR  ZU  HOCH    ");
}

void MainWindow::ofenZuKaltSignal(){

    schreibeFehlerCode(" TEMPERATUR  ZU  NIEDRIG    ");
}

void MainWindow::zutatNichtAufLagerSignal(){

    schreibeFehlerCode(" FEHLENDE ZUTATEN    ");
}

void MainWindow::schreibeFehlerCode(QString alleFehler)
{
this->label16->setStyleSheet("QLabel { background-color: red }");
fehlerGesamt.append(alleFehler);
this->label18->setText(fehlerGesamt);

}

MainWindow::~MainWindow()
{
    delete baecker;
}
