#include "Zutat.h"

std::string Zutat::getString(int zutat)
{
    switch (zutat) {
    case Zutat::milch:
        return "Milch";
    case Zutat::eier:
        return "Eier";
    case Zutat::backpulver:
        return "Backpulver";
    case Zutat::zucker:
        return "Zucker";
    case Zutat::nuesse:
        return "Nüsse";
    case Zutat::mehl:
        return "Mehl";
    case Zutat::zuckerguss:
        return "Zuckerguss";
    case Zutat::schokoguss:
        return "Schokoguss";
    case Zutat::streusel:
        return "Streusel";
    default : return "Leer";

    }
}
