#pragma once
#include <string>

class Teig{
public:
    enum {
        blaetterteig, hefeteig, ruehrteig
    };

    static std::string getString(int teig);

};
