#pragma once
#include <string>

class Zutat{
public:
    enum
    {
        milch, eier, backpulver,
        zucker, nuesse, mehl,
        zuckerguss, schokoguss, streusel
    };

    static std::string getString(int zutat);

};


