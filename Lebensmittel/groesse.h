#ifndef GROESSE_H
#define GROESSE_H
#include <string>

class Groesse
{
public:

    enum {
        ein, zwei, drei
    };

    static std::string getString(int groesse);

};


#endif // GROESSE_H
