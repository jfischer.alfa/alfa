#ifndef FORM_H
#define FORM_H
#include <string>

class Form
{

public:
    enum {
        herz, quadrat, kreis
    };

    static std::string getString(int form);

};



#endif // FORM_H
