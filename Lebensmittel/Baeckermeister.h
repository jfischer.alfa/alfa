#pragma once
#include <map>
#include "Auftrag/auftrag.h"
#include "Rezept/rezept.h"
#include "Belag.h"
#include "Teig.h"
#include "Zutat.h"
#include "form.h"
#include "groesse.h"
using namespace std;

class Baeckermeister
{
private:
    /// Berechnen Zutaten für ein Plätzchen Anhand Auftrag
    map<int, double> berechneZutatenFuerEins(Auftrag &auftrag);

    /// Berechnen Zuteten für die benötigte Anzahl der Plätzchen
    map<int, double> berechneZutatenStueckzahl(map<int, double> map, int stueckzahl);

    /// Berechnen der Backzeit in Sekunden
    double berechneBackzeit(int teig);

    /// Berechnen der Backtemperatur in Celsius
    double berechneBacktemperatur(int teig);

public:

    Baeckermeister();
    Rezept getRezept(Auftrag &auftrag);

    ~Baeckermeister();

};
