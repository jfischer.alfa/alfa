#include "Teig.h"

std::string Teig::getString(int teig)
{
    switch (teig) {
    case Teig::blaetterteig:
        return "Blätterteig";
    case Teig::hefeteig:
        return "Hefeteig";
    case Teig::ruehrteig:
        return "Ruehrteig";
    default : return "Leer";

    }
}

