#include "groesse.h"

std::string Groesse::getString(int groesse)
{
    switch (groesse) {
    case Groesse::ein:
        return "Ein";
    case Groesse::zwei:
        return "Zwei";
    case Groesse::drei:
        return "Drei";
    default : return "Leer";

    }
}

