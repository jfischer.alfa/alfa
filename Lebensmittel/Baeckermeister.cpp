#include "Baeckermeister.h"

map<int, double> Baeckermeister::berechneZutatenFuerEins(Auftrag &auftrag)
{

    map<int, double> zutatenListe;

    switch (auftrag.getBelag()) {

    case Belag::amerikaner:
        zutatenListe.insert(pair<int, double>(Zutat::zuckerguss, 50.3));
        zutatenListe.insert(pair<int, double>(Zutat::schokoguss, 30.99));
        break;

    case Belag::zuckerstreusel:
        zutatenListe.insert(pair<int, double>(Zutat::streusel, 20.1));
        break;

    case Belag::zuckerglasur:
        zutatenListe.insert(pair<int, double>(Zutat::zuckerguss, 70.4));
        break;

    }

    switch (auftrag.getTeigsorte()) {

    case Teig::blaetterteig:
        zutatenListe.insert(pair<int, double>(Zutat::backpulver, 30.3));
        zutatenListe.insert(pair<int, double>(Zutat::zucker, 15.0));
        zutatenListe.insert(pair<int, double>(Zutat::eier, 3.0));
        break;

    case Teig::hefeteig:
        zutatenListe.insert(pair<int, double>(Zutat::backpulver, 25.6));
        zutatenListe.insert(pair<int, double>(Zutat::zucker, 25.7));
        zutatenListe.insert(pair<int, double>(Zutat::nuesse, 17.9));
        break;

    case Teig::ruehrteig:
        zutatenListe.insert(pair<int, double>(Zutat::mehl, 60.8));
        zutatenListe.insert(pair<int, double>(Zutat::zucker, 45.1));
        zutatenListe.insert(pair<int, double>(Zutat::milch, 52.2));
        break;
    }

    return zutatenListe;
}

map<int, double> Baeckermeister::berechneZutatenStueckzahl(map<int, double> mapEinzel, int stueckzahl)
{
    map<int, double> mapmitStueckzahlBeruecksichtigt;

    for (pair<int, double> p : mapEinzel) {

        double pMalgenommen = p.second * stueckzahl;

        pair<int, double> stueckzahlberuecksichtigt{ p.first,pMalgenommen };

        mapmitStueckzahlBeruecksichtigt.insert(stueckzahlberuecksichtigt);
    }

    return mapmitStueckzahlBeruecksichtigt;
}

double Baeckermeister::berechneBackzeit(int teig)
{
    switch (teig) {
    case Teig::hefeteig:
        return 5.0;
    case Teig::ruehrteig:
        return 6.1;
    case Teig::blaetterteig:
        return 7.3;
    default: return 0.0;
    }
}

double Baeckermeister::berechneBacktemperatur(int teig)
{
    switch (teig) {
    case Teig::hefeteig:
        return 150.3;
    case Teig::ruehrteig:
        return 130.7;
    case Teig::blaetterteig:
        return 170.5;
    default: return 0.0;
    }
}


Baeckermeister::Baeckermeister()
{
}

Rezept Baeckermeister::getRezept(Auftrag &auftrag)
{
    map<int, double> liste = Baeckermeister::berechneZutatenFuerEins(auftrag);
    map<int, double> zutatenliste = Baeckermeister::berechneZutatenStueckzahl(liste, auftrag.getStueckzahl());
    double backzeit = Baeckermeister::berechneBackzeit(auftrag.getTeigsorte());
    double backtemperatur = Baeckermeister::berechneBacktemperatur(auftrag.getTeigsorte());

    return Rezept(backtemperatur, backzeit, zutatenliste);
}

Baeckermeister::~Baeckermeister()
{
}
