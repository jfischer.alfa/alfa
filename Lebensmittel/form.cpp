#include "form.h"

std::string Form::getString(int form)
{
    switch (form){
    case Form::herz:
        return "Herz";
    case Form::quadrat:
        return "Quadrat";
    case Form::kreis:
        return "Kreis";
    default : return "Leer";
    }

}
