#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include <QTextEdit>
#include "Controller/baeckereicontroller.h"
#include "Lebensmittel/Belag.h"
#include "Lebensmittel/Teig.h"
#include "Lebensmittel/Zutat.h"
#include "Lebensmittel/form.h"
#include "Lebensmittel/groesse.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    // Interface
    BaeckereiController* baecker;

    //Seitentitel
    QLabel* label;
    //Teigsorten
    QPushButton* pushButton;
    QPushButton* pushButton1;
    QPushButton* pushButton2;
    QLabel* label1;
    //Plätzchenform
    QPushButton* pushButton3;
    QPushButton* pushButton4;
    QPushButton* pushButton5;
    QLabel* label2;
    //Plätzchengroesse
    QPushButton* pushButton6;
    QPushButton* pushButton7;
    QPushButton* pushButton8;
    QLabel* label3;
    //Stückzahl
    QPushButton* pushButton9;
    QPushButton* pushButton10;
    QPushButton* pushButton11;
    QLabel* label4;
    //Verzierung
    QPushButton* pushButton12;
    QPushButton* pushButton13;
    QPushButton* pushButton14;
    QLabel* label5;



    //AuftragsSummary

    QLabel* label6;
    QLabel* label7;
    QLabel* label8;
    QLabel* label9;
    QLabel* label10;

    int teig{-1};
    int belag{-1};
    int form{-1};
    int groesse{-1};
    int stueckzahl{-1};

    QString l6;
    QString l7;
    QString l8;
    QString l9;
    QString l10;

    //Produktion starten
    QPushButton* pushButton15;

    //Restock
    QPushButton* pushButton16;

    //Auftrags Status

    QLabel* label11;
    QLabel* label12;
    QLabel* label13;
    QLabel* label16;
    QLabel* label17;
    QLabel* label18;

    QString fehlerGesamt{""};

public:
    explicit MainWindow(QWidget *parent = 0);
    void fensterdesign();
    ~MainWindow();

private:
    void schreibeFehlerCode(QString);



private slots:

    void schreibenB();
    void schreibenB1();
    void schreibenB2();
    void schreibenB3();
    void schreibenB4();
    void schreibenB5();
    void schreibenB6();
    void schreibenB7();
    void schreibenB8();
    void schreibenB9();
    void schreibenB10();
    void schreibenB11();
    void schreibenB12();
    void schreibenB13();
    void schreibenB14();

    //Erzeugen und verarbeiten des Auftrags
    void auftrag();
    void auftragsnummer(int);
    void backenbeendet();
    void lagerGefuellt();

    //Erzeugen der Fehlermeldungen
    void backzeitZuHochSignal();
    void backzeitZuNiedrigSignal();
    void ofenZuHeissSignal();
    void ofenZuKaltSignal();
    void zutatNichtAufLagerSignal();

};

#endif // MAINWINDOW_H
